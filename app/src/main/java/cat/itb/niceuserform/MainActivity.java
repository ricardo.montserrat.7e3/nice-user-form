package cat.itb.niceuserform;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity
{
    public static HashMap<String, String> users = new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.login_button).setOnClickListener(x -> startActivity(new Intent(MainActivity.this, Login.class)));
        findViewById(R.id.register_button).setOnClickListener(x -> startActivity(new Intent(MainActivity.this, Register.class)));
    }
}