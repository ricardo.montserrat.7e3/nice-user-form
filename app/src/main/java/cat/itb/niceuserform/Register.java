package cat.itb.niceuserform;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.widget.Checkable;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

public class Register extends AppCompatActivity
{
    TextInputEditText usernameInput, passwordInput, repeatedPasswordInput, emailInput, nameInput, surnamesInput, birthInput;
    Checkable terms;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        usernameInput = findViewById(R.id.username_input_edit);
        passwordInput = findViewById(R.id.password_input_edit);

        repeatedPasswordInput = findViewById(R.id.repeated_password_input);
        emailInput = findViewById(R.id.email_input_edit);

        nameInput = findViewById(R.id.name_input_edit);
        surnamesInput = findViewById(R.id.surnames_input_edit);

        birthInput = findViewById(R.id.birth_input_edit);

        terms = findViewById(R.id.terms_checkbox);

        findViewById(R.id.login_button).setOnClickListener(x ->
        {
            startActivity(new Intent(Register.this, Login.class)); finish();
        });

        findViewById(R.id.register_button).setOnClickListener(x -> register());

        instantiateDatePicker();
    }

    private void register()
    {
        if (!Login.setErrorSuccessfully(usernameInput, "Please, write your username")
                && !Login.setErrorSuccessfully(passwordInput, "Please, write your password")
                && !Login.setErrorSuccessfully(repeatedPasswordInput, "Please, write your password")
                && !Login.setErrorSuccessfully(emailInput, "Please, write your email")
                && !Login.setErrorSuccessfully(nameInput, "Please, write your name")
                && !Login.setErrorSuccessfully(surnamesInput, "Please, write your surname"))
        {
            String password = getInputText(passwordInput);
            if (!getInputText(repeatedPasswordInput).equals(getInputText(passwordInput))) repeatedPasswordInput.setError("It's not the same as the first password set!");
            else if(password.length() < 8) passwordInput.setError("The password must have at least 8 characters!");
            else if(!terms.isChecked()) Toast.makeText(Register.this, "Please, accept the terms if you want to continue", Toast.LENGTH_LONG).show();
            else
            {
                MainActivity.users.put(getInputText(usernameInput), getInputText(passwordInput));
                finish();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void instantiateDatePicker()
    {
        final Calendar myCalendar = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) ->
        {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            String dateText = myCalendar.get(Calendar.DAY_OF_MONTH) + " / " + (myCalendar.get(Calendar.MONTH) + 1) + " / " + myCalendar.get(Calendar.YEAR);
            birthInput.setText(dateText);
        };

        birthInput.setOnClickListener(v ->
            new DatePickerDialog(Register.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show());
    }

    private String getInputText(TextInputEditText input) { return Objects.requireNonNull(input.getText()).toString(); }
}