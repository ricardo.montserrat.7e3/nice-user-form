package cat.itb.niceuserform;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

public class Login extends AppCompatActivity
{
    private TextInputEditText usernameInput, passwordInput;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameInput = findViewById(R.id.username_input_edit);
        passwordInput = findViewById(R.id.password_input_edit);

        findViewById(R.id.login_button).setOnClickListener(x ->
        {
            if(setErrorSuccessfully(usernameInput, "Please, write your Username") || setErrorSuccessfully(passwordInput, "Please, write your Password")) return;

            assert usernameInput.getText() != null && passwordInput.getText() != null;

            String username = usernameInput.getText().toString();
            if (!MainActivity.users.containsKey(username)) usernameInput.setError("Username doesn't exist!");
            else if (!passwordInput.getText().toString().equals(MainActivity.users.get(username))) passwordInput.setError("The user password is not correct");
            else
            {
                startActivity(new Intent(Login.this, Welcome.class));
                finish();
            }
        });
        findViewById(R.id.register_button).setOnClickListener(x -> { startActivity(new Intent(Login.this, Register.class)); finish(); });

        findViewById(R.id.forgot_password).setOnClickListener(x -> Toast.makeText(Login.this, "Good luck LMAO", Toast.LENGTH_SHORT).show());
    }

    public static boolean setErrorSuccessfully(TextInputEditText textInput, String error)
    {
        if(Objects.requireNonNull(textInput.getText()).toString().isEmpty())
        {
            textInput.setError(error);
            return true;
        }
        return false;
    }
}